package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.data.access.CachedGetBalanceProvider;
import com.manumaqueda.account.transfer.data.access.AccountCreator;
import com.manumaqueda.account.transfer.data.access.CachedAccountCreator;
import com.manumaqueda.account.transfer.data.access.GetBalancesProvider;
import com.manumaqueda.account.transfer.data.model.Account;
import com.manumaqueda.account.transfer.data.model.AccountRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuelmaqueda on 17 September 2018
 */
public class TestDAFactory {

    public static Account createAccount(String name){
        AccountCreator da = new CachedAccountCreator();
        AccountRequest bob = new AccountRequest(name);
        return da.create(bob);
    }
    public static List<Account> createSeveralAccount(String... names){
        AccountCreator da = new CachedAccountCreator();
        List<Account> accounts = new ArrayList<>();
        for(String name: names){
            AccountRequest account = new AccountRequest(name);
            accounts.add(da.create(account));
        }
        return accounts;
    }
    public static List<Account> getAllBalances(){
        GetBalancesProvider getBalancesProvider = new CachedGetBalanceProvider();
        return getBalancesProvider.getAllAccountBalances();
    }
    public static Account getSingleBalances(String accountId){
        GetBalancesProvider getBalancesProvider = new CachedGetBalanceProvider();
        return getBalancesProvider.getAccountBalance(accountId);
    }

}
