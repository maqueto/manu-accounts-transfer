package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import com.manumaqueda.account.transfer.data.access.CachedGetBalanceProvider;
import com.manumaqueda.account.transfer.data.model.Account;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;

/**
 * Created by manuelmaqueda on 17 September 2018
 */
public class GetBalanceAccountService {
    private GetBalanceService getBalanceService;

    @Before
    public void setUp(){
        ManuAccountTransfer.mAccounts = new HashMap<>();
        this.getBalanceService = new GetBalanceService(new CachedGetBalanceProvider());
    }

    @Test
    public void getAllBalanceZeroCash(){
        // when created two account
        TestDAFactory.createSeveralAccount("Bob", "Alice");
        // retrieve balance for all the accounts
        Response output = this.getBalanceService.getAllBalances();
        List<Account> accounts = ((List)output.getEntity());
        Assert.assertEquals("should return status 200", 200, output.getStatus());
        Assert.assertEquals("Should have 2 accounts as a response",accounts.size(),2);
        for(Account acc: accounts){
            Assert.assertTrue("Balance should be zero", acc.getBalance() == 0.0);
        }
    }

    @Test
    public void getSingleZeroBalance(){
        // when Bob, Alice and Peter are created as accounts
        List<Account> accounts = TestDAFactory.createSeveralAccount("Bob", "Alice","Peter");
        Account alice = null;
        for(Account acc: accounts){
            if(acc.getOwnerName().equalsIgnoreCase("Alice")) {
                alice = acc;
                break;
            }
        }
        // retrieve balance for alice
        Response output = this.getBalanceService.getAccountBalance(alice.getId());
        Assert.assertEquals("Accounts Ids should be the same",alice.getId(),
                ((Account)output.getEntity()).getId());
    }
}
