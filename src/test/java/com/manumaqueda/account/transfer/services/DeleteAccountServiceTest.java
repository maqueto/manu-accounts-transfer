package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import com.manumaqueda.account.transfer.data.access.CachedAccountDeleter;
import com.manumaqueda.account.transfer.data.model.Account;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.HashMap;

/**
 * Created by manuelmaqueda on 17 September 2018
 */
public class DeleteAccountServiceTest {

    private DeleteAccountService service;

    @Before
    public void setUp(){
        service = new DeleteAccountService(new CachedAccountDeleter());
        ManuAccountTransfer.mAccounts = new HashMap<>();
    }

    @Test
    public void removeExistingAccount(){
        Account bob = TestDAFactory.createAccount("Bob");
        Response output = service.deleteAccount(bob.getId());
        Assert.assertEquals("should return status 200", 200, output.getStatus());
        Assert.assertEquals("Returned accounts should have the same id as original",
                bob.getId(), ((Account)output.getEntity()).getId());
    }

    @Test
    public void tryRemoveNonExistingAccount(){
        //when account created and deleted
        Account bob = TestDAFactory.createAccount("Bob");
        service.deleteAccount(bob.getId());
        //then delete again
        Response output = service.deleteAccount(bob.getId());
        Assert.assertNull("Entity should be null",  output.getEntity());
    }
}
