package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import com.manumaqueda.account.transfer.data.access.CachedTransferOperation;
import com.manumaqueda.account.transfer.data.model.Account;
import com.manumaqueda.account.transfer.data.model.TransferRequest;
import com.manumaqueda.account.transfer.data.model.TransferResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import javax.ws.rs.core.Response;
import java.util.HashMap;

/**
 * Created by manuelmaqueda on 17 September 2018
 *
 *
 */
public class TransferMoneyServiceTest {
    private TransferMoneyService transferMoneyService;

    @Before
    public void setUp(){
        this.transferMoneyService = new TransferMoneyService(new CachedTransferOperation());
        ManuAccountTransfer.mAccounts = new HashMap<>();
    }

    @Test
    public void doDeposit(){
        // when bob is created
        Account bob = TestDAFactory.createAccount("Bob");
        // then perform deposit
        TransferRequest transferRequest = new TransferRequest(null,bob.getId(),
                "first deposit to bob",1000);
        Response output = this.transferMoneyService.transfer(transferRequest);
        Assert.assertEquals("Should return 200",200,output.getStatus());
        Assert.assertTrue("Message is ok",((TransferResponse)output.getEntity()).getMsg().equalsIgnoreCase("ok"));
        Assert.assertTrue("Balance should be 1000 for Bob",
                ((TransferResponse)output.getEntity()).getToAccount().getBalance() == 1000.0);
    }

    @Test
    public void transferMoney(){
        // when Bob, Alice and Peter are created with 1000, 500, 0 respectively
        Account bob = TestDAFactory.createAccount("Bob");
        Account alice = TestDAFactory.createAccount("Alice");
        Account peter = TestDAFactory.createAccount("Peter");
        TransferRequest depositToBob = new TransferRequest(null,bob.getId(),
                "first deposit to Bob",1000);
        TransferRequest depositToAlice = new TransferRequest(null,alice.getId(),
                "first deposit to Alice",500);
        this.transferMoneyService.transfer(depositToBob);
        this.transferMoneyService.transfer(depositToAlice);
        // Bob transfer 500 to Peter
        TransferRequest transferRequest = new TransferRequest(bob.getId(), peter.getId(),"Lets be nice and have fair shares",500);
        this.transferMoneyService.transfer(transferRequest);
        // All the accounts should have 500
        Account resultBob = TestDAFactory.getSingleBalances(bob.getId());
        Account resultAlice = TestDAFactory.getSingleBalances(alice.getId());
        Account resultPeter = TestDAFactory.getSingleBalances(peter.getId());
        Assert.assertTrue("Bob balance should be 500.0",resultBob.getBalance() == 500.0);
        Assert.assertTrue("Alice balance should be 500.0",resultAlice.getBalance() == 500.0);
        Assert.assertTrue("Peter balance should be 500.0",resultPeter.getBalance() == 500.0);

    }

    @Test
    public void errorWrongToAccount(){
        TransferRequest transferRequest = new TransferRequest(null,"AAAAAAAAA",
                "lets hijack money to account",1000);
        Response output = this.transferMoneyService.transfer(transferRequest);
        Assert.assertTrue("Response not OK", !((TransferResponse)output.getEntity()).getMsg().equalsIgnoreCase("ok"));
    }
    @Test
    public void errorNegativeAmount(){
        Account bob = TestDAFactory.createAccount("Bob");
        TransferRequest transferRequest = new TransferRequest(null,bob.getId(),
                "lets deposit a negative amount",-1000);
        Response output = this.transferMoneyService.transfer(transferRequest);
        Assert.assertTrue("Response not OK", !((TransferResponse)output.getEntity()).getMsg().equalsIgnoreCase("ok"));
    }
    @Test
    public void errorNoSubject(){
        Account bob = TestDAFactory.createAccount("Bob");
        TransferRequest transferRequest = new TransferRequest(null,bob.getId(),
                "",1000);
        Response output = this.transferMoneyService.transfer(transferRequest);
        Assert.assertTrue("Response not OK", !((TransferResponse)output.getEntity()).getMsg().equalsIgnoreCase("ok"));
    }
    @Test
    public void errorNoToAccount(){
        TransferRequest transferRequest = new TransferRequest(null,null,
                "Transfer to no-one",1000);
        Response output = this.transferMoneyService.transfer(transferRequest);
        Assert.assertTrue("Response not OK", !((TransferResponse)output.getEntity()).getMsg().equalsIgnoreCase("ok"));
    }
}
