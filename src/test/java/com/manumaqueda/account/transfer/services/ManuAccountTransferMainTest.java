package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by manuelmaqueda on 18 September 2018
 */
public class ManuAccountTransferMainTest {

    @Before
    public void setUp(){
        ManuAccountTransfer.mAccounts = new HashMap<>();
    }

    @Test
    public void test(){
        try{
            // program starts and shutdowns
            ManuAccountTransfer.main();
        }catch (IOException e){}

    }
}
