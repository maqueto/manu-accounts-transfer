package com.manumaqueda.account.transfer;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

import com.manumaqueda.account.transfer.services.TransferMoneyService;
import com.manumaqueda.account.transfer.data.access.CachedAccountCreator;
import com.manumaqueda.account.transfer.data.access.CachedAccountDeleter;
import com.manumaqueda.account.transfer.data.access.CachedGetBalanceProvider;
import com.manumaqueda.account.transfer.data.access.CachedTransferOperation;
import com.manumaqueda.account.transfer.services.CreateAccountService;
import com.manumaqueda.account.transfer.services.DeleteAccountService;
import com.manumaqueda.account.transfer.services.GetBalanceService;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import com.manumaqueda.account.transfer.data.model.Account;


/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class ManuAccountTransfer {
    public static final String BASE_URI = "http://localhost:8080/manu-account-transfers/";
    public static HashMap<String, Account> mAccounts = new HashMap<>();
    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig()
                .registerInstances(
                        new CreateAccountService(new CachedAccountCreator()),
                        new DeleteAccountService(new CachedAccountDeleter()),
                        new GetBalanceService(new CachedGetBalanceProvider()),
                        new TransferMoneyService(new CachedTransferOperation())
                );
        rc.register(JacksonFeature.class);
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }


    public static void main(String... args)throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Starting Manu-Account-Transfer server at "+ BASE_URI));
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Shutting down Manu-Account-Transfer");
            }
        });
    }
}
