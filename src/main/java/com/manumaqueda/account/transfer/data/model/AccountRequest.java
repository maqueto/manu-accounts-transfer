package com.manumaqueda.account.transfer.data.model;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class AccountRequest {
    private String ownerName;

    public AccountRequest() {
    }
    public AccountRequest(String name){ ownerName = name;}

    public String getOwnerName() {
        return ownerName;
    }

}
