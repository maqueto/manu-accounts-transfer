package com.manumaqueda.account.transfer.data.model;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class Account {
    private String id;
    private double balance;
    private String ownerName;
    private Instant createdDate;
    private List<TransferOperation> transferHistory;

    public Account(AccountRequest request) {
        this.ownerName = request.getOwnerName();
        this.id = UUID.randomUUID().toString();
        this.balance = 0.0;
        this.createdDate = Instant.now();
        this.transferHistory = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getCreatedDate() {
        return DateTimeFormatter.ISO_INSTANT.format(createdDate);
    }

    public List<TransferOperation> getTransferHistory() {
        return transferHistory;
    }

    public void performOperation(TransferType type, double amount, String relatedAccount, String subject) {
        if(type.equals(TransferType.POSITIVE))
            this.balance += amount;
        else
            this.balance -= amount;
        this.transferHistory.add(new TransferOperation(type,relatedAccount,subject,amount));
    }
}
