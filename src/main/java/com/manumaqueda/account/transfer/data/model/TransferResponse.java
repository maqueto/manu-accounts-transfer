package com.manumaqueda.account.transfer.data.model;

/**
 * Created by manuelmaqueda on 17 September 2018
 */
public class TransferResponse {
    private String msg;
    private Account fromAccount;
    private Account toAccount;

    public TransferResponse(String msg, Account fromAccount, Account toAccount) {
        this.msg = msg;
        this.fromAccount = fromAccount;
        this.toAccount = toAccount;
    }

    public String getMsg() {
        return msg;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public Account getToAccount() {
        return toAccount;
    }
}
