package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import com.manumaqueda.account.transfer.data.model.Account;
import com.manumaqueda.account.transfer.data.model.AccountRequest;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class CachedAccountCreator implements AccountCreator {
    @Override
    public Account create(AccountRequest accountRequest) {
        Account account = new Account(accountRequest);
        ManuAccountTransfer.mAccounts.put(account.getId(),account);
        return account;
    }
}
