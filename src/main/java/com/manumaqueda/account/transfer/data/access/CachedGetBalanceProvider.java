package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import com.manumaqueda.account.transfer.data.model.Account;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class CachedGetBalanceProvider implements GetBalancesProvider{
    @Override
    public Account getAccountBalance(String id) {
        return ManuAccountTransfer.mAccounts.get(id);
    }

    @Override
    public List<Account> getAllAccountBalances() {
        return new ArrayList<>(ManuAccountTransfer.mAccounts.values());
    }
}
