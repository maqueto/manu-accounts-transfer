package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.data.model.Account;

import java.util.List;

public interface GetBalancesProvider {
    Account getAccountBalance(String id);
    List<Account> getAllAccountBalances();
}
