package com.manumaqueda.account.transfer.data.model;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class TransferRequest {
    private double amount;
    private String fromAccountId;
    private String toAccountId;
    private String subject;

    public TransferRequest() {}

    public TransferRequest(String fromAccountId, String toAccountId, String subject,double amount) {
        this.amount = amount;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.subject = subject;
    }

    public double getAmount() {
        return amount;
    }

    public String getFromAccountId() {
        return fromAccountId;
    }

    public String getToAccountId() {
        return toAccountId;
    }

    public String getSubject() {
        return subject;
    }
}
