package com.manumaqueda.account.transfer.data.model;

import java.time.Instant;
import java.time.format.DateTimeFormatter;

/**
 * Created by manuelmaqueda on 17 September 2018
 */
public class TransferOperation {
    private TransferType type;
    private String relatedAccountId;
    private String subject;
    private double amount;
    private Instant timestamp;

    public TransferOperation(TransferType type, String relatedAccountId, String subject, double amount) {
        this.type = type;
        this.relatedAccountId = relatedAccountId;
        this.subject = subject;
        this.amount = amount;
        this.timestamp = Instant.now();
    }

    public String getTimestamp() {
        return DateTimeFormatter.ISO_INSTANT.format(timestamp);
    }

    public TransferType getType() {
        return type;
    }

    public String getRelatedAccountId() {
        return relatedAccountId;
    }

    public String getSubject() {
        return subject;
    }

    public double getAmount() {
        return amount;
    }
}
