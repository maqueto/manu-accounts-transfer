package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import com.manumaqueda.account.transfer.data.model.Account;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class CachedAccountDeleter implements AccountDeleter {
    @Override
    public Account delete(String id) {
        return ManuAccountTransfer.mAccounts.remove(id);
    }
}
