package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.data.model.TransferRequest;
import com.manumaqueda.account.transfer.data.model.TransferResponse;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public interface TransferOperation {
    TransferResponse transfer(TransferRequest request);
}
