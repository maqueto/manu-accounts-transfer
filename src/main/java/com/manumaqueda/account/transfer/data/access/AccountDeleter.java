package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.data.model.Account;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public interface AccountDeleter {
    Account delete(String id);
}
