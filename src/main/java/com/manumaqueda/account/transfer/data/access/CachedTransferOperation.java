package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.ManuAccountTransfer;
import com.manumaqueda.account.transfer.data.model.TransferRequest;
import com.manumaqueda.account.transfer.data.model.TransferResponse;
import com.manumaqueda.account.transfer.data.model.TransferType;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
public class CachedTransferOperation implements TransferOperation {
    @Override
    public TransferResponse transfer(TransferRequest tRequest) {
        String check = checkTransfer(tRequest);
        if(!check.isEmpty())
            return new TransferResponse(check,null,null);
        else{
            if(tRequest.getFromAccountId() != null)
                ManuAccountTransfer.mAccounts.get(tRequest.getFromAccountId())
                        .performOperation(TransferType.NEGATIVE,tRequest.getAmount(),tRequest.getToAccountId(),tRequest.getSubject());
            ManuAccountTransfer.mAccounts.get(tRequest.getToAccountId())
                    .performOperation(TransferType.POSITIVE,tRequest.getAmount(),tRequest.getFromAccountId(),tRequest.getSubject());
            System.out.println("Transfer done with subject "+tRequest.getSubject());
            return new TransferResponse("OK",
                    (tRequest.getFromAccountId() != null) ?
                            ManuAccountTransfer.mAccounts.get(tRequest.getFromAccountId()) : null,
                    ManuAccountTransfer.mAccounts.get(tRequest.getToAccountId()));
        }
    }
    private String checkTransfer(TransferRequest tRequest){
        StringBuilder error = new StringBuilder();
        if(tRequest.getAmount() <0)
            error.append("Transfer amount needs to be positive.");
        else if(tRequest.getToAccountId() == null || !ManuAccountTransfer.mAccounts.containsKey(tRequest.getToAccountId()))
            error.append("The destination account doesn't exist.");
        else if(tRequest.getFromAccountId() != null && !ManuAccountTransfer.mAccounts.containsKey(tRequest.getFromAccountId()))
            error.append("The source account is invalid, please use a valid existing account.");
        else if(tRequest.getSubject().isEmpty())
            error.append("Subject is a mandatory parameter, please fill it.");
        return error.toString();
    }
}
