package com.manumaqueda.account.transfer.data.access;

import com.manumaqueda.account.transfer.data.model.Account;
import com.manumaqueda.account.transfer.data.model.AccountRequest;

public interface AccountCreator {
    Account create(AccountRequest accountRequest);
}
