package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.data.access.AccountDeleter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
@Path("/account")
public class DeleteAccountService {
    AccountDeleter da;
    public DeleteAccountService(AccountDeleter deleter) {
        this.da = deleter;
    }

    @DELETE @Path("/{id}") @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAccount(@PathParam("id") String accountId){
        return Response.status(200).entity(da.delete(accountId)).build();
    }
}
