package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.data.access.TransferOperation;
import com.manumaqueda.account.transfer.data.model.TransferRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
@Path("/transfer")
public class TransferMoneyService {
    TransferOperation da;
    public TransferMoneyService(TransferOperation to){
        this.da = to;
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfer(TransferRequest tRequest){
        return Response.status(200).entity(da.transfer(tRequest)).build();
    }
}
