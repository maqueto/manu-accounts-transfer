package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.data.access.AccountCreator;
import com.manumaqueda.account.transfer.data.model.AccountRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
@Path("/account")
public class CreateAccountService {
    AccountCreator da;
    public CreateAccountService(AccountCreator da) {
        this.da = da;
    }
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAccount(AccountRequest aRequest){
        return Response.status(200).entity(da.create(aRequest)).build();
    }
}
