package com.manumaqueda.account.transfer.services;

import com.manumaqueda.account.transfer.data.access.GetBalancesProvider;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by manuelmaqueda on 14 September 2018
 */
@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class GetBalanceService {
    GetBalancesProvider da;
    public GetBalanceService(GetBalancesProvider provider) {
        this.da = provider;
    }
    @GET
    public Response getAllBalances(){
        System.out.println("Entering in the all balance method");
        try{
            return Response.status(200).entity(this.da.getAllAccountBalances()).build();
        }catch (Exception e){
            System.out.println("Error ->"+e.getMessage());
            return Response.status(400).entity("Error retrieving account balances").build();
        }

    }
    @GET
    @Path("/{id}")
    public Response getAccountBalance(@PathParam("id") String accountId) {
        try{
            return Response.status(200).entity(this.da.getAccountBalance(accountId)).build();
        }catch (Exception e){
            return Response.status(400).entity("Error retrieving balance for account -> "+accountId).build();
        }
    }
}
