#manu-accounts-transfer

This is a demo account transfer server that allows existing accounts to perform transfers between them. This server also
 provides functionality to create/remove accounts from the system and to retrieve account balances.
 
##Services
* CreateAccountService [POST /account]. This service creates a new account in the system. It requires just a name that 
it is used as the account name, and it initialises in the system the rest of the necessary metadata for the account:
 account id, balance, created date and transfer history. I also returns that information.
* DeleteAccountService - [DELETE /account/{id}] - Deletes the account with {id} from the system and returns the snapshot
of the deleted account. 
* TransferAccountService - [POST /transfer] - It performs a transfer between two existing accounts in the system. It also
allows to do transfers from an 'anonymous' account to a system account in order to initialise account balances. It expects
a transfer request and retrieves the balance of the related accounts after the transfer is performed.
* GetBalance 
    * GetAccountBalance [GET /accounts/{id}] - retrieve the balance and rest of information for the account with {id}
    * GetBalances [GET /accounts] - retrieves details of all the accounts in the system.
    
##Assumptions
* The solution built is currency agnostic and therefore no multi-currency functionality is included.
* Transfers can be done without a 'from' account in order to allow other systems to initialize the balance of the system 
accounts.
* The account balance after a transfer can be negative, as the system does not specify penalties for those use cases.
* Tests are built to ensure the business logic of the program is preserved, and none particular tests are built against
 the Jersey framework.
 
##Example

Run test and build the program using `mvn clean package` 
Start the HTTP server running  `java -cp  target/manu-accounts-transfer-jar-with-dependencies.jar com.manumaqueda.account.transfer.ManuAccountTransfer`

Bob, Alice and Peter example. Please remember to replace `{bobId}`, `{aliceId}` and `{peterId}`, for their respective ids retrieved by the API.

1. Create 3 accounts: Bob, Alice, Peter
    - `curl -i -X POST -H 'Content-Type: application/json' http://localhost:8080/manu-account-transfers/account -d '{"ownerName": "Bob"}'`
    - `curl -i -X POST -H 'Content-Type: application/json' http://localhost:8080/manu-account-transfers/account -d '{"ownerName": "Alice"}'`
    - `curl -i -X POST -H 'Content-Type: application/json' http://localhost:8080/manu-account-transfers/account -d '{"ownerName": "Peter"}'`

2. Transfer (deposit) 1000 and 500 to Bob and Alice respectively.
    - `curl -i -X POST http://localhost:8080/manu-account-transfers/transfer  -H 'Content-Type: application/json' -d '{"amount": 1000,"fromAccountId": null,"toAccountId": "{bobId}","subject": "First deposit of 1000 to Bob"}'`
    - `curl -i -X POST http://localhost:8080/manu-account-transfers/transfer  -H 'Content-Type: application/json' -d '{"amount": 500,"fromAccountId": null,"toAccountId": "{aliceId}","subject": "First deposit of 500 to Alice"}'`

3. Get All balances
    - `curl -i -X GET http://localhost:8080/manu-account-transfers/accounts` 

4. Transfer 500 from Bob to Peter to ensure they have same balances
    - `curl -i -X POST http://localhost:8080/manu-account-transfers/transfer  -H 'Content-Type: application/json' -d '{"amount": 500,"fromAccountId": "{bobId}","toAccountId": "{peterId}","subject": "Lets be fair and share the same money among shareholders"}'`

5. Get Bob and Peter account details to ensure their balances are now 500
    - `curl -i -X GET http://localhost:8080/manu-account-transfers/accounts/{bobId}` 
    - `curl -i -X GET http://localhost:8080/manu-account-transfers/accounts/{peterId}`


6. Lets transfer all funds from Bob to Alice as he is leaving the company, remove his account and check all the balances
    - `curl -i -X POST http://localhost:8080/manu-account-transfers/transfer  -H 'Content-Type: application/json' -d '{"amount": 500,"fromAccountId": "{bobId}","toAccountId": "{aliceId}","subject": "Bob amortization"}'`
    - `curl -i -X DELETE http://localhost:8080/manu-account-transfers/account/{bobId}`
    - `curl -i -X GET http://localhost:8080/manu-account-transfers/accounts`




  